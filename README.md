This is  directory contains everything you need to setup a development environment of the Circular Content project

## Requirements: 
- [Git](https://git-scm.com/) for version control of files & documents. Tested with 1.9.1  
- [Virtualbox](http://virtualbox.org) for virtual machines. Tested with 5.0.10
- [Vagrant] (https://vagrantup.com) for managing virtual machines. Tested with 1.7.4
- Vagrant plugins (vagrant install plugin _name of plugin_: 
  - [vagrant-hostmanager](https://github.com/smdahlen/vagrant-hostmanager) for automagically setting up local domain names. Tested with 1.6.1
- [Ansible](http://ansible.com) for automagically provisioning a virtual machine. tested with 1.9.4

## Usage
1. Install git, Virtualbox, Vagrant (including plugins) and Ansible 
2. git clone this repo
