<?php 
define( 'WP_DEBUG', true );
if( WP_DEBUG ) {
  @ini_set( 'log_errors', 'On' );
  @ini_set( 'display_errors', 'Off' );
  define( 'WP_DEBUG_DISPLAY', false );
  define( 'SCRIPT_DEBUG', true );  
  define( 'SAVEQUERIES', true );
}

/*
 * Use the snippet below in your footer to check the queries 
if ( current_user_can( 'administrator' ) ) {
    global $wpdb;
    echo "<pre>";
    print_r( $wpdb->queries );
    echo "</pre>";
}
**/
?>
