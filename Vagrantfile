# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network :private_network, ip: "192.168.33.10"
  config.vm.hostname = "circularcontent.tmp"
  config.vm.define "circularcontent-devbox"  
  
  # We need the vagrant-hostmanager to deal with the domain names of VM
  unless Vagrant.has_plugin?("vagrant-hostmanager")
    puts "\n"
    puts "------ WARNING: INSTALL PLUGIN: VAGRANT-HOSTMANAGER ------"
    puts "I'm using Vagrant-Hostmanager, https://github.com/smdahlen/vagrant-hostmanager"
    puts "It's for managing the /etc/hosts file on guests within a multi-machine environment."
    puts "This enables you to access a domain name associated with guest vm #1 from guest vm #2."
    puts "It also manages your host machine's /etc/hosts file to have access the guest VMs"
    puts "Install it by typing on the command-line: "
    puts "vagrant plugin install vagrant-hostmanager"
    puts "\n"
    exit
  end

  if Vagrant.has_plugin?("vagrant-hostmanager") 
    config.hostmanager.enabled = true     # use the host-manager plugin
    config.hostmanager.manage_host = true # change your host machine /etc/hosts as well
    config.hostmanager.aliases =  %w(www.circularcontent.tmp) #  keep in mind wildcards are not supported due to using /etc/hosts! 
  end 


  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  
  # Only mount the plugins, themes and languages folder with our repo, but make sure they cannot be changed from the guest OS
  config.vm.synced_folder "../www/wp-content/plugins", "/var/www/circularcontent.tmp/wp-content/plugins", owner: "www-data", group: "www-data", mount_options: ["dmode=775,fmode=664"]
  config.vm.synced_folder "../www/wp-content/themes", "/var/www/circularcontent.tmp/wp-content/themes", owner: "www-data", group: "www-data", mount_options: ["dmode=775,fmode=664"]
  #config.vm.synced_folder "../www/wp-content/languages", "/var/www/circularcontent.tmp/wp-content/languages", owner: "www-data", group: "www-data", mount_options: ["dmode=775,fmode=664"]
  #config.vm.synced_folder "import/files/uploads", "/var/www/circularcontent.tmp/wp-content/uploads", owner: "www-data", group: "www-data", mount_options: ["dmode=777,fmode=777"]

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   config.vm.provider "virtualbox" do |vb|
     # Customize the amount of memory on the VM:
     vb.memory = "1024"
   end
  
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "provisioning/playbook.yml"
  end
end

